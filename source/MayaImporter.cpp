#include <algorithm>
#include "MayaImporter.hpp"
#include "picojson.h"

using namespace std;

namespace _3dcg_json {

MayaImporter::MayaImporter(std::string filename) {
	ifstream file(filename, ios::in | ios::binary | ios::ate);
	json_text.clear();
	uint64_t file_size = file.tellg();
	file.seekg(0, ios::beg);
	vector<char> bytes(file_size);
	file.read(&bytes[0], file_size);
	json_text = string(&bytes[0], file_size);
}

void MayaImporter::parseFile() {
	picojson::value values;
	auto err = parse(values, json_text);
	if (!err.empty()) {
		cerr << err << endl;
		return;
	}

	vector<Face> face_indices;

	if (values.contains("faces")) {
		auto face_values = values.get("faces").get<picojson::array>()[0].get<picojson::array>();
		for (unsigned int i = 0; i < face_values.size(); i += 11) {
			uint32_t val = (uint32_t)face_values[i].get<double>();
			if (val == 42) {
				Face face;
				face.vert_idx.x = (uint32_t)face_values[i + 1].get<double>();
				face.vert_idx.y = (uint32_t)face_values[i + 2].get<double>();
				face.vert_idx.z = (uint32_t)face_values[i + 3].get<double>();
				face.mesh_idx = (uint32_t)face_values[i + 4].get<double>();
				face.uv_idx.x = (uint32_t)face_values[i + 5].get<double>();
				face.uv_idx.y = (uint32_t)face_values[i + 6].get<double>();
				face.uv_idx.z = (uint32_t)face_values[i + 7].get<double>();
				face.normal_idx.x = (uint32_t)face_values[i + 8].get<double>();
				face.normal_idx.y = (uint32_t)face_values[i + 9].get<double>();
				face.normal_idx.z = (uint32_t)face_values[i + 10].get<double>();
				face_indices.push_back(face);
			}
		}
		cout << "Face count (int3): " << face_indices.size() << endl;
	} else {
		cout << "No face indices found in this file." << endl;
	}

	vector<Material> materials;

	if (values.contains("materials")) {
		auto material_list = values.get("materials").get<picojson::array>();
		Material material;
		for (auto mat : material_list) {
			auto diffuse = mat.get("colorDiffuse").get<picojson::array>();
			material.diffuse.x = (float)diffuse[0].get<double>();
			material.diffuse.y = (float)diffuse[1].get<double>();
			material.diffuse.z = (float)diffuse[2].get<double>();

			auto ambient = mat.get("colorAmbient").get<picojson::array>();
			material.ambient.x = (float)ambient[0].get<double>();
			material.ambient.y = (float)ambient[1].get<double>();
			material.ambient.z = (float)ambient[2].get<double>();

			auto specular = mat.get("colorSpecular").get<picojson::array>();
			material.specular.x = (float)specular[0].get<double>();
			material.specular.y = (float)specular[1].get<double>();
			material.specular.z = (float)specular[2].get<double>();

			material.specular_coefficient = (float)mat.get("specularCoef").get<double>();
			material.texture = mat.get("mapDiffuse").get<string>();
			material.transparency = (float)mat.get("transparency").get<double>();
			material.setting.diffuse_coefficient = (float)mat.get("setting").get("mqoDiffuseCoef").get<double>();
			material.setting.emissive = (float)mat.get("setting").get("mqoEmissive").get<double>();
			material.setting.shader = (float)mat.get("setting").get("mqoShader").get<double>();
			material.setting.alpha_map = mat.get("setting").get("mqoMapAlpha").get<string>();
			material.setting.bump_map = mat.get("setting").get("mqoMapBump").get<string>();
			material.wrap_repeat = mat.get("mapDiffuseWrap").get<picojson::array>()[0].get<string>().compare("repeat") == 0;

			materials.push_back(material);
		}
	} else {
		cout << "No materials found in this file." << endl;
	}

	if (values.contains("normals")) {
		auto normal_values = values.get("normals").get<picojson::array>()[0].get<picojson::array>();
		for (unsigned int i = 0; i < normal_values.size(); i += 3) {
			vec3<float> n;
			n.x = (float)normal_values[i + 0].get<double>();
			n.y = (float)normal_values[i + 1].get<double>();
			n.z = (float)normal_values[i + 2].get<double>();
			normals.push_back(n);
		}
		cout << "Normals count (float3): " << normals.size() << endl;
	} else {
		cout << "No normals found in this file." << endl;
	}

	if (values.contains("uvs")) {
		auto uv_values = values.get("uvs").get<picojson::array>()[0].get<picojson::array>()[0].get<picojson::array>();
		for (unsigned int i = 0; i < uv_values.size(); i += 2) {
			vec2<float> uv;
			uv.x = (float)uv_values[i + 0].get<double>();
			uv.y = (float)uv_values[i + 1].get<double>();
			uvs.push_back(uv);
		}
		cout << "UV coordinates (float2): " << uvs.size() << endl;
	} else {
		cout << "No UV coordinates found in this file." << endl;
	}

	if (values.contains("vertices")) {
		auto vertex_values = values.get("vertices").get<picojson::array>()[0].get<picojson::array>();
		for (unsigned int i = 0; i < vertex_values.size(); i += 3) {
			vec3<float> vert;
			vert.x = (float)vertex_values[i + 0].get<double>();
			vert.y = (float)vertex_values[i + 1].get<double>();
			vert.z = (float)vertex_values[i + 2].get<double>();
			vertices.push_back(vert);
		}
		cout << "Vertex count (float3): " << vertices.size() << endl;
	} else {
		cout << "No vertices found in this file." << endl;
	}

	int num_meshes = face_indices.back().mesh_idx;
	meshes = vector<Mesh>(num_meshes);

	for (auto& face : face_indices) {
		int mesh_id = face.mesh_idx - 1;
		meshes[mesh_id].id = mesh_id;

		meshes[mesh_id].tri_indices.push_back(face.vert_idx);

		meshes[mesh_id].vtx_indices.emplace(face.vert_idx.x);
		meshes[mesh_id].vtx_indices.emplace(face.vert_idx.y);
		meshes[mesh_id].vtx_indices.emplace(face.vert_idx.z);

		meshes[mesh_id].nrm_indices.emplace(face.normal_idx.x);
		meshes[mesh_id].nrm_indices.emplace(face.normal_idx.y);
		meshes[mesh_id].nrm_indices.emplace(face.normal_idx.z);

		meshes[mesh_id].uv_indices.emplace(face.uv_idx.x);
		meshes[mesh_id].uv_indices.emplace(face.uv_idx.y);
		meshes[mesh_id].uv_indices.emplace(face.uv_idx.z);
	}

	for (auto& mesh : meshes) {
		mesh.material = materials[mesh.id + 1];
	}
}

void MayaImporter::importModel() {
	for (auto& mesh : meshes) {
		mesh.normalizeTriangleIndices();
		mesh.flattenIndices();
		importMesh(mesh);
	}
}

void MayaImporter::importMesh(const Mesh& mesh) {
	MFloatPointArray vertexArray;
	for (auto vtx_idx : mesh.vtx_indices) {
		auto vtx = vertices[vtx_idx];
		vertexArray.append(vtx.x, vtx.y, vtx.z);
	}

	MIntArray polygonCount;
	for (unsigned int i = 0; i < mesh.tri_indices.size(); ++i) {
		polygonCount.append(3);
	}

	MIntArray polygonConnects;
	for (auto& tri : mesh.tri_indices) {
		polygonConnects.append(tri.x);
		polygonConnects.append(tri.y);
		polygonConnects.append(tri.z);
	}

	MVectorArray vertexNorms;
	for (auto nrm_idx : mesh.nrm_indices) {
		auto nrm = normals[nrm_idx];
		vertexNorms.append(MVector(nrm.x, nrm.y, nrm.z));
	}

	MFloatArray Us, Vs;
	for (auto uv_idx : mesh.uv_indices) {
		auto uv = uvs[uv_idx];
		Us.append(uv.x);
		Vs.append(uv.y);
	}
	
	MFnMesh meshFn;
	meshFn.create(vertexArray.length(), polygonCount.length(), vertexArray, polygonCount, polygonConnects, Us, Vs);
	meshFn.setName(mesh.name.c_str());
	meshFn.setVertexNormals(vertexNorms, polygonConnects);
	meshFn.assignUVs(polygonCount, polygonConnects);

	MDagPath meshPath = MDagPath::getAPathTo(meshFn.object());
	meshPath.extendToShape();

	importMaterial(meshPath, mesh);
}

void MayaImporter::importMaterial(MDagPath& meshPath, const Mesh& mesh) {
	auto mat = mesh.material;
	MColor diffuse(mat.diffuse.x, mat.diffuse.y, mat.diffuse.z);
	MColor ambient(mat.ambient.x, mat.ambient.y, mat.ambient.z);
	MColor specular(mat.specular.x, mat.specular.y, mat.specular.z);

	MFnPhongShader phong;
	phong.create();
	phong.setName(mat.texture.c_str());
	phong.setColor(diffuse);
	phong.setAmbientColor(ambient);
	phong.setSpecularColor(specular);
	phong.setCosPower(mat.specular_coefficient);
	phong.setDiffuseCoeff(mat.setting.diffuse_coefficient);
	phong.setTranslucenceCoeff(mat.transparency);

	// Create shading group
	MFnSet set;
	MSelectionList sel;
	sel.add(meshPath);
	set.create(sel, MFnSet::kRenderableOnly, false);
	set.setName("shadingGroup");

	// Break default connection that is created.
	MPlugArray plugArray;
	MPlug surfaceShader = set.findPlug("surfaceShader", true);
	surfaceShader.connectedTo(plugArray, true, true);
	MDGModifier dgModifier;
	dgModifier.disconnect(plugArray[0], surfaceShader);

	// Connect outcolor to DG modifier
	dgModifier.connect(phong.findPlug("outColor"), surfaceShader);

	cout << "Reading texture " << mat.texture << endl;

	//create a texture node
	MString file_name(mat.texture.c_str());
	MFnDependencyNode nodeFn;
	MObject obj = nodeFn.create(MString("file"), file_name);

	// Create file object
	MFileObject file;
	file.setName(file_name);
	if (!file.exists()) {
		return;
	}

	// Find the plug for texture files and set its path.
	nodeFn.findPlug(MString("ftn"), true).setValue(file.fullName());

	// Get global textures list
	MItDependencyNodes nodeIt(MFn::kTextureList);
	MObject texture_list = nodeIt.thisNode();
	MFnDependencyNode slFn;
	slFn.setObject(texture_list);

	MPlug textures = slFn.findPlug(MString("textures"));

	// Loop until an available connection is found
	MPlug element;
	int next = 0;
	while (true) {
		element = textures.elementByLogicalIndex(next);
		if (!element.isConnected())
			break;
		next++;
	}

	// Connect '.message' plug of render node to "shaders"/"textures" plug of default*List
	MPlug message = nodeFn.findPlug(MString("message"), true);
	dgModifier.connect(message, element);
	dgModifier.doIt();

	dgModifier.connect(nodeFn.findPlug("outColor"), phong.findPlug("color"));

		// Create 2D Texture Placement
	MFnDependencyNode tp2dFn;
	tp2dFn.create("place2dTexture", "2dTexture");
	tp2dFn.findPlug("wrapU").setValue(true);
	tp2dFn.findPlug("wrapV").setValue(true);

	// Connect all the 18 things
	dgModifier.connect(tp2dFn.findPlug("coverage"), nodeFn.findPlug("coverage"));
	dgModifier.connect(tp2dFn.findPlug("mirrorU"), nodeFn.findPlug("mirrorU"));
	dgModifier.connect(tp2dFn.findPlug("mirrorV"), nodeFn.findPlug("mirrorV"));
	dgModifier.connect(tp2dFn.findPlug("noiseUV"), nodeFn.findPlug("noiseUV"));
	dgModifier.connect(tp2dFn.findPlug("offset"), nodeFn.findPlug("offset"));
	dgModifier.connect(tp2dFn.findPlug("outUV"), nodeFn.findPlug("uvCoord"));
	dgModifier.connect(tp2dFn.findPlug("outUvFilterSize"), nodeFn.findPlug("uvFilterSize"));
	dgModifier.connect(tp2dFn.findPlug("repeatUV"), nodeFn.findPlug("repeatUV"));
	dgModifier.connect(tp2dFn.findPlug("rotateFrame"), nodeFn.findPlug("rotateFrame"));
	dgModifier.connect(tp2dFn.findPlug("rotateUV"), nodeFn.findPlug("rotateUV"));
	dgModifier.connect(tp2dFn.findPlug("stagger"), nodeFn.findPlug("stagger"));
	dgModifier.connect(tp2dFn.findPlug("translateFrame"), nodeFn.findPlug("translateFrame"));
	dgModifier.connect(tp2dFn.findPlug("vertexCameraOne"), nodeFn.findPlug("vertexCameraOne"));
	dgModifier.connect(tp2dFn.findPlug("vertexUvOne"), nodeFn.findPlug("vertexUvOne"));
	dgModifier.connect(tp2dFn.findPlug("vertexUvTwo"), nodeFn.findPlug("vertexUvTwo"));
	dgModifier.connect(tp2dFn.findPlug("vertexUvThree"), nodeFn.findPlug("vertexUvThree"));
	dgModifier.connect(tp2dFn.findPlug("wrapU"), nodeFn.findPlug("wrapU"));
	dgModifier.connect(tp2dFn.findPlug("wrapV"), nodeFn.findPlug("wrapV"));

	dgModifier.doIt();
}

void Mesh::normalizeTriangleIndices() {
	offset = numeric_limits<unsigned int>::max();
	for (auto& tri_idx : tri_indices) {
		offset = offset < tri_idx.x ? offset : tri_idx.x;
		offset = offset < tri_idx.y ? offset : tri_idx.y;
		offset = offset < tri_idx.z ? offset : tri_idx.z;
	}

	for (auto& tri_idx : tri_indices) {
		tri_idx.x -= offset;
		tri_idx.y -= offset;
		tri_idx.z -= offset;
	}
}

void Mesh::flattenIndices() {
	set<unsigned int> indices;
	largest = 0;
	for (auto& tri_idx : tri_indices) {
		largest = max(largest, tri_idx.x);
		largest = max(largest, tri_idx.y);
		largest = max(largest, tri_idx.z);
		
		indices.emplace(tri_idx.x);
		indices.emplace(tri_idx.y);
		indices.emplace(tri_idx.z);
	}

	vector<unsigned int> index;
	index.resize(largest + 1, 0);
	unsigned int counter = 0;
	for (auto i : indices) {
		index[i] = counter;
		counter++;
	}

	for (auto& tri_idx : tri_indices) {
		tri_idx.x = index[tri_idx.x];
		tri_idx.y = index[tri_idx.y];
		tri_idx.z = index[tri_idx.z];
	}
}

} // namespace