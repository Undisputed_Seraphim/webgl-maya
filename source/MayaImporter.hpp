#pragma once

#include <vector>
#include <set>
#include <string>
#include <map>

#include "MayaIncludes.hpp"
#include "Translator.hpp"

namespace _3dcg_json {

template <typename T>
struct vec2 { T x, y; };

template <typename T>
struct vec3 { T x, y, z; };

struct Texture {
	float diffuse_coefficient, emissive, shader;
	std::string alpha_map, bump_map;
};

struct Material {
	vec3<float> diffuse, ambient, specular;
	float specular_coefficient;
	float transparency;
	std::string texture;
	Texture setting;
	bool wrap_repeat;
};

struct Face {
	vec3<uint32_t> vert_idx;
	int mesh_idx;
	vec3<uint32_t> uv_idx;
	vec3<uint32_t> normal_idx;
};

class Mesh {
public:
	Mesh() : id(-1), name("") {}

	void normalizeTriangleIndices();
	void flattenIndices();

	int id;
	std::string name;
	Material material;

	std::vector<vec3<uint32_t>> tri_indices;
	std::set<uint32_t> vtx_indices;
	std::set<uint32_t> nrm_indices;
	std::set<uint32_t> uv_indices;

	unsigned int offset;
	unsigned int largest;
};

class MayaImporter {
public:
	MayaImporter(std::string filename);
	~MayaImporter() {}

	void parseFile();
	void importModel();
	
private:
	void importMesh(const Mesh& mesh);
	void importMaterial(MDagPath& meshPath, const Mesh& mesh);

	std::string json_text;

	std::vector<Mesh> meshes;

	std::vector<vec3<float>> normals;
	std::vector<vec3<float>> vertices;
	std::vector<vec2<float>> uvs;
};

} // namespace