#include <maya/MFnPlugin.h>
#include "Translator.hpp"

#define VENDOR ""
#define NAME "3DCG-Arts JSON data importer"
#define SCRIPTPATH "none" // else "none"
#define IMAGEPATH "none"

   /**
	* initializePlugin() contains the code to register any commands,
	* tools, devices, and so on, defined by the plug-in with Maya.
	* It is called only once mmediately after the plug-in is loaded.
	* @param obj MObject used internally by Maya
	*/
MStatus __declspec(dllexport) initializePlugin( MObject obj ) {
	MStatus status;
	MFnPlugin exportPlugin( obj, VENDOR, _3dcg_json::Translator::getVersion().asChar(), "Any" );

	// register the translator with the system(name, image, creator func., mel script)
	status = exportPlugin.registerFileTranslator( NAME,
		//IMAGEPATH,
		NULL,
		_3dcg_json::Translator::creator,
		//SCRIPTPATH,
		NULL,
		"",
		true );

	if ( !status ) {
		status.perror( "registerFileTranslator" );
		return status;
	}

	return status;
}

/**
 * The unitializePlugin() function contains the code necessary to
 * de-register from Maya whatever was registered through initializePlugin()
 * @param obj MObject used internally by Maya
 */
MStatus __declspec(dllexport) uninitializePlugin( MObject obj ) {
	MStatus status;
	MFnPlugin exportPlugin( obj );

	status = exportPlugin.deregisterFileTranslator( NAME );

	if ( !status ) {
		status.perror( "deregisterFileTranslator" );
		return status;
	}

	return status;
}