#include "Translator.hpp"

using namespace std;

namespace _3dcg_json {

MString Translator::mVersionNumber("1.0");
int Translator::mHighVersion(0);
int Translator::mLowVersion(1);

void* Translator::creator() {
	return new Translator();
}

MStatus Translator::reader(const MFileObject& file, const MString& options, MPxFileTranslator::FileAccessMode mode) {
	MStatus status = MS::kFailure;

	string fileName = file.fullName().asChar();

	// try to open the file
	ifstream jsonFile(fileName, ifstream::binary);
	if (!jsonFile) {
		cerr << fileName << " could not be opened for reading." << endl;
		return MS::kFailure;
	}

	MayaImporter importer(fileName);

	try {
		importer.parseFile();
	} catch (const exception &e) {
		cerr << "There was an error parsing the JSON file." << endl;
		cerr << e.what() << endl;
		return MS::kFailure;
	}

	try {
		importer.importModel();
	} catch (const exception &e) {
		cerr << "There was an error reading model data into Maya." << endl;
		cerr << e.what() << endl;
		return MS::kFailure;
	}
	
	return MS::kSuccess;
}

MStatus Translator::writer(const MFileObject& file, const MString& options, MPxFileTranslator::FileAccessMode mode) {
	MStatus status = MS::kFailure;
	return status;
}

MPxFileTranslator::MFileKind Translator::identifyFile(const MFileObject & file, const char *buffer, short size) const {
	MFileKind rVal = MFileKind::kNotMyFileType;

	string name(file.name().asChar());

	if ((name.size() > 4) && (name.find(".json") != string::npos)) {
		rVal = kCouldBeMyFileType;
	}

	ifstream xpsFile(name, ifstream::binary);
	if (!xpsFile) {
		cout << file.name() << " could not be opened for reading." << endl;
	}

	return rVal;
}

} // namespace