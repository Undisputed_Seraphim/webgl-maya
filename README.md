# WebGL to Maya importer

## Todo:
- UV still incorrectly read into Maya. How to fix? Hmm.

#### You will need:
- CMake
- Autodesk Maya

#### Compilation (Windows only):
- `mkdir build && cd build`
- `cmake .. -DMAYA_VERSION=2015 -G "Visual Studio 14 2015 Win64"`
- Note that compilation must be 64-bit
- Replace the value after `-DMAYA_VERSION=` with whatever version of Maya you are using.

#### Usage after compilation
- In Maya, go to Windows -> Settings/Preferences -> Plugin Manager
- In the new window, click on Browse and navigate to the compiled `.mll` file.
- Check the "Loaded" box to load it.
- Import the file via the normal import interface in Maya.
- Alternatively, add the path to the output `.mll` files to your `Maya.env` file, using the variable `MAYA_PLUG_IN_PATH = `.